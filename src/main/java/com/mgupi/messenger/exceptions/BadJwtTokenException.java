package com.mgupi.messenger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Bad jwt token")
public class BadJwtTokenException extends RuntimeException{
}
