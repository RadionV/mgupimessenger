package com.mgupi.messenger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Recipient not found")
public class RecipientNotFoundException extends RuntimeException{
}
