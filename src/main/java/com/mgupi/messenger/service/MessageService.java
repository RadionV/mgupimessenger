package com.mgupi.messenger.service;

import com.mgupi.messenger.dto.message.MessageReceiveDto;
import com.mgupi.messenger.dto.message.MessageSendDto;
import com.mgupi.messenger.dto.notify.MessageNotify;
import com.mgupi.messenger.entity.Chat;
import com.mgupi.messenger.entity.CustomUser;
import com.mgupi.messenger.entity.Message;
import com.mgupi.messenger.exceptions.AccessDeniedException;
import com.mgupi.messenger.exceptions.BadJwtTokenException;
import com.mgupi.messenger.exceptions.MessageNotFoundException;
import com.mgupi.messenger.exceptions.RecipientNotFoundException;
import com.mgupi.messenger.repository.ChatRepository;
import com.mgupi.messenger.repository.CustomUserRepository;
import com.mgupi.messenger.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class MessageService {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final JWTUtil jwtUtil;
    private final CustomUserRepository customUserRepository;
    private final ChatService chatService;
    private final ChatRepository chatRepository;
    private final MessageRepository messageRepository;
    private final SimpUserRegistry simpUserRegistry;

    @Autowired
    public MessageService(SimpMessagingTemplate simpMessagingTemplate, JWTUtil jwtUtil, CustomUserRepository customUserRepository, com.mgupi.messenger.service.ChatService chatService, ChatRepository chatRepository, MessageRepository messageRepository, SimpUserRegistry simpUserRegistry) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.jwtUtil = jwtUtil;
        this.customUserRepository = customUserRepository;
        this.chatService = chatService;
        this.chatRepository = chatRepository;
        this.messageRepository = messageRepository;
        this.simpUserRegistry = simpUserRegistry;
    }

    public MessageReceiveDto getMessage(UUID id, String username){

        Message message = messageRepository.findById(id).orElseThrow(MessageNotFoundException::new);

        if(! (message.getRecipient().getUsername().equals(username) || message.getSender().getUsername().equals(username) )){
            throw new AccessDeniedException();
        }

        MessageReceiveDto messageReceiveDto = new MessageReceiveDto();
        messageReceiveDto.setChatId(message.getChat().getId());
        messageReceiveDto.setSenderId(message.getSender().getId());
        messageReceiveDto.setContent(message.getContent());
        messageReceiveDto.setSendTime(message.getTimestamp());
        return messageReceiveDto;
    }

    // add transaction
    @Transactional
    public void sendMessage(MessageSendDto messageDto, String jwtTokenWithBearer){

        String jwtToken = jwtTokenWithBearer.substring(7);

        if(!jwtUtil.validateToken(jwtToken)){
            throw new BadJwtTokenException();
        }

        if(customUserRepository.findByUsername(messageDto.getRecipient()) == null){
            throw new RecipientNotFoundException();
        }

        CustomUser recipient = customUserRepository.findByUsername(messageDto.getRecipient());
        CustomUser sender = customUserRepository.findByUsername(jwtUtil.getLoginFromToken(jwtToken));
        Chat chat = chatService.createOrFindChatByUsers(recipient, sender);

        Message message = new Message(
                sender,
                recipient,
                chat,
                messageDto.getContent(),
                new Date()
        );

        if ( simpUserRegistry.getUser(recipient.getUsername()) != null){
            message.setDelivered(true);
        }else{
            message.setDelivered(false);
        }

        Message persistMessage = messageRepository.save(message);

        sendMessage(persistMessage);

    }

    public void sendMessage(Message message){
        simpMessagingTemplate.convertAndSendToUser(
                message.getRecipient().getUsername(),
                "/messages",
                new MessageNotify(
                        message.getId()
                )
        );
    }

    public void sendAllNotDeliveredMessage(String username){
        CustomUser user = customUserRepository.findByUsername(username);

        List<Message> notDeliveredMessages = messageRepository.
                findAllByRecipientIdAndDelivered(user.getId(), false);

        for (Message message : notDeliveredMessages){
            sendMessage(message);
            message.setDelivered(true);
            messageRepository.save(message);
        }
    }
}
