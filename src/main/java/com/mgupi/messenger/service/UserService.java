package com.mgupi.messenger.service;

import com.mgupi.messenger.dto.auth.AuthResponseDto;
import com.mgupi.messenger.dto.user.UserDto;
import com.mgupi.messenger.dto.registration.RegistrationRequestDto;
import com.mgupi.messenger.entity.CustomUser;
import com.mgupi.messenger.exceptions.UsernameAlreadyExistsException;
import com.mgupi.messenger.repository.CustomUserRepository;
import org.apache.catalina.User;
import org.hibernate.id.insert.IdentifierGeneratingInsert;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private final CustomUserRepository customUserRepository;
    private final PasswordEncoder passwordEncoder;
    private final JWTUtil jwtUtil;

    public UserService(CustomUserRepository customUserRepository, PasswordEncoder passwordEncoder, JWTUtil jwtUtil) {
        this.customUserRepository = customUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtil = jwtUtil;
    }

    public CustomUser registrationNewUser(RegistrationRequestDto registrationRequestDto){
        // add transaction
        CustomUser customUser = new CustomUser();

        if(customUserRepository.findByUsername(registrationRequestDto.getUsername()) != null){
            throw new UsernameAlreadyExistsException();
        }

        customUser.setUsername(registrationRequestDto.getUsername());
        customUser.setPassword(registrationRequestDto.getPassword());
        customUser.setRole("USER");
        customUser.setFirstName(registrationRequestDto.getFirstName());
        customUser.setLastName(registrationRequestDto.getLastName());
        customUser.setPatronymic(registrationRequestDto.getPatronymic());
        customUser.setPhoneNumber(registrationRequestDto.getPhoneNumber());
        customUser.setDepartment(registrationRequestDto.getDepartment());

        CustomUser persistUser = customUserRepository.save(customUser);

        return persistUser;
    }

    public AuthResponseDto findUserByLoginAndPassword(String username, String password){
        CustomUser user = customUserRepository.findByUsername(username);
        if (user != null && user.getPassword().equals(password)){
            return new AuthResponseDto(jwtUtil.generateToken(user.getUsername()));
        }else{
            return new AuthResponseDto("None");
        }
    }

    public Boolean checkUserByUsername(String username){
        return customUserRepository.findByUsername(username) == null;
    }

    public UserDto getSelfInfo(String username){
        CustomUser user = customUserRepository.findByUsername(username);
        UserDto userDto = new UserDto();

        userDto.setPatronymic(user.getPatronymic());
        userDto.setDepartment(user.getDepartment());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setPhoneNumber(user.getPhoneNumber());
        return userDto;
    }

    public List<String> findAllUsersByUsername(String username){

        List<CustomUser> userList = customUserRepository.findByUsernameLike("%" + username + "%");

        List<String> usernameList = new ArrayList<>();

        for(CustomUser customUser : userList){
            usernameList.add(customUser.getUsername()) ;
        }
        return usernameList;
    }

    public void updateUserFirstName(String username, String firstName){
        CustomUser user = customUserRepository.findByUsername(username);
        user.setFirstName(firstName);
        customUserRepository.save(user);
    }

    public void updateLastName(String username, String lastName){
        CustomUser user = customUserRepository.findByUsername(username);
        user.setLastName(lastName);
        customUserRepository.save(user);
    }

    public void updatePatronymic(String username, String patronymic){
        CustomUser user = customUserRepository.findByUsername(username);
        user.setPatronymic(patronymic);
        customUserRepository.save(user);
    }

    public void updatePhoneNumber(String username, String phoneNumber){
        CustomUser user = customUserRepository.findByUsername(username);
        user.setPhoneNumber(phoneNumber);
        customUserRepository.save(user);
    }

    public void updateDepartment(String username, String phoneNumber){
        CustomUser user = customUserRepository.findByUsername(username);
        user.setDepartment(phoneNumber);
        customUserRepository.save(user);
    }
}





