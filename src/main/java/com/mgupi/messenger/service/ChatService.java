package com.mgupi.messenger.service;

import com.mgupi.messenger.dto.chat.ChatInfoDto;
import com.mgupi.messenger.dto.message.MessageReceiveDto;
import com.mgupi.messenger.entity.Chat;
import com.mgupi.messenger.entity.CustomUser;
import com.mgupi.messenger.entity.Message;
import com.mgupi.messenger.repository.ChatRepository;
import com.mgupi.messenger.repository.CustomUserRepository;
import com.mgupi.messenger.repository.MessageRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class ChatService {

    private final ChatRepository chatRepository;
    private final CustomUserRepository customUserRepository;
    private final MessageRepository messageRepository;

    public ChatService(ChatRepository chatRepository, CustomUserRepository customUserRepository, MessageRepository messageRepository) {
        this.chatRepository = chatRepository;
        this.customUserRepository = customUserRepository;
        this.messageRepository = messageRepository;
    }

    @Transactional
    public Chat createOrFindChatByUsers(CustomUser user1, CustomUser user2){

        UUID chatIdByUsers = chatRepository.findChatByUsers(user1.getId(), user2.getId());

        if(chatIdByUsers != null){
        // chat exists
            return chatRepository.findById(chatIdByUsers).get();
        }else{
            // chat does not exists
            Chat chat = new Chat();
            Set<CustomUser> customUserSet = new HashSet<>();
            customUserSet.add(user1);
            customUserSet.add(user2);
            chat.setCustomUserSet(customUserSet);
            chatRepository.save(chat);
            return chat;
        }
    }

    public List<ChatInfoDto> findAllChatByUser(String username){

        List<ChatInfoDto> chatInfoDtos = new ArrayList<>();

        CustomUser customUser = customUserRepository.findByUsername(username);

        Set<Chat> allChatByUser = customUser.getChatSet();

        for(Chat chat : allChatByUser){
            ChatInfoDto chatInfoDto = new ChatInfoDto();
            chatInfoDto.setChatId(chat.getId());
            Set<CustomUser> customUserSet = chat.getCustomUserSet();

            for (CustomUser chatUser : customUserSet) {
                if (!chatUser.getUsername().equals(username)) {
                    chatInfoDto.setFirstName(chatUser.getFirstName());
                    chatInfoDto.setLastName(chatUser.getLastName());
                    chatInfoDto.setPatronymic(chatUser.getPatronymic());
                    chatInfoDto.setUserId(chatUser.getId());
                    chatInfoDto.setUsername(chatUser.getUsername());
                    chatInfoDtos.add(chatInfoDto);
                }
            }
        }


        return chatInfoDtos;
    }
    
    public List<MessageReceiveDto> getAllMessagesByChat(UUID chatId, String username){

        Chat chat = chatRepository.findById(chatId).get();

        List<Message> messageList = chat.getMessageList();

        List<MessageReceiveDto> messageListDto = new ArrayList<>();

        for(Message message : messageList){
            MessageReceiveDto messageReceiveDto = new MessageReceiveDto();

            messageReceiveDto.setContent(message.getContent());
            messageReceiveDto.setSendTime(message.getTimestamp());
            messageReceiveDto.setChatId(message.getChat().getId());
            messageReceiveDto.setSenderId(message.getSender().getId());
            messageListDto.add(messageReceiveDto);
        }

        return messageListDto;
    }

    public List<MessageReceiveDto> getPageMessagesByChat(UUID chatId, Integer page, String username){

        Pageable pageable = PageRequest.of(page, 5);
        List<Message> messagePage = messageRepository.findMessagesByChatId(chatId, pageable);
        System.out.println("Size of meessage list : " + messagePage.size());

        List<MessageReceiveDto> messages = new ArrayList<>();

        for(Message message : messagePage){
            MessageReceiveDto messageReceiveDto = new MessageReceiveDto();

            messageReceiveDto.setContent(message.getContent());
            messageReceiveDto.setSendTime(message.getTimestamp());
            messageReceiveDto.setChatId(message.getChat().getId());
            messageReceiveDto.setSenderId(message.getSender().getId());
            messages.add(messageReceiveDto);
        }

        return messages;
    }





}
