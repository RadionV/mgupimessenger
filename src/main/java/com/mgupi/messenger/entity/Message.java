package com.mgupi.messenger.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Message {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @ManyToOne
    @JoinColumn(name="sender_id")
    @JsonManagedReference
    private CustomUser sender;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="recipient_id")
    private CustomUser recipient;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="chat_id")
    private Chat chat;

    @Column(name = "content")
    private String content;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date timestamp;

    private Boolean delivered;

    public Message(CustomUser sender, CustomUser recipient, Chat chat, String content, Date timestamp) {
        this.sender = sender;
        this.recipient = recipient;
        this.chat = chat;
        this.content = content;
        this.timestamp = timestamp;
    }
}
