package com.mgupi.messenger.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class CustomUser {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "department")
    private String department;

    @OneToMany(mappedBy = "sender", fetch = FetchType.EAGER)
    @JsonBackReference
    private List<Message> senderMessages;

    @OneToMany(mappedBy = "recipient", fetch = FetchType.EAGER)
    @JsonBackReference
    private List<Message> recipientMessages;

    @ManyToMany(mappedBy = "customUserSet", fetch = FetchType.EAGER)
    @JsonBackReference
    private Set<Chat> chatSet;

}
