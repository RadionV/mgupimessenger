package com.mgupi.messenger.repository;

import com.mgupi.messenger.entity.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CustomUserRepository extends JpaRepository<CustomUser, UUID> {
    CustomUser findByUsername(String username);

    List<CustomUser> findByUsernameLike(String username);

}
