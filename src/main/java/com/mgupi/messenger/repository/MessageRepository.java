package com.mgupi.messenger.repository;

import com.mgupi.messenger.entity.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MessageRepository extends JpaRepository<Message, UUID> {

    @Query(value = "SELECT * FROM message WHERE recipient_id = ?1 and delivered = ?2", nativeQuery = true )
    List<Message> findAllByRecipientIdAndDelivered(UUID recipientId, Boolean delivered);

    List<Message> findMessagesByChatId(UUID chatId, Pageable page);
}
