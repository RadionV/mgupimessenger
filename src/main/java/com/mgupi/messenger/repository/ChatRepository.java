package com.mgupi.messenger.repository;

import com.mgupi.messenger.entity.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ChatRepository extends JpaRepository<Chat, UUID> {

    @Query(value="select Cast(chat_id as varchar) " +
            "from custom_user_chat " +
            "where custom_user_id = :user1 " +
            "INTERSECT " +
            "select Cast(chat_id as varchar) " +
            "from custom_user_chat " +
            "where custom_user_id = :user2", nativeQuery=true)
    UUID findChatByUsers(@Param("user1") UUID user1, @Param("user2") UUID user2);

    @Query(value = "select Cast(chat_id as varchar) " +
            "from custom_user_chat " +
            "where custom_user_id = :user",
            nativeQuery = true)
    List<Chat> findAllChatsByUser(@Param("user") UUID user);

}
