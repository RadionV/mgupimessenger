package com.mgupi.messenger.controller;

import com.mgupi.messenger.dto.message.MessageReceiveDto;
import com.mgupi.messenger.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class HelloController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/")
    public String hello() {
        return "Hello";
    }

    // сюда доступ разрешен только user и admin
    @GetMapping("/user")
    public String user() {
        return "User";
    }

}