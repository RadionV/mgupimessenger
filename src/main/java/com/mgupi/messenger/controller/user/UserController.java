package com.mgupi.messenger.controller.user;

import com.mgupi.messenger.dto.user.UserDto;
import com.mgupi.messenger.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/api/user")
    public UserDto getSelfInfo(Principal principal){
        return userService.getSelfInfo(principal.getName());
    }

    @GetMapping("/api/user/find/{username}")
    public List<String> findAllUsersByUsername(@PathVariable(name = "username") String username){
        return userService.findAllUsersByUsername(username);
    }

    @PutMapping("/api/user/firstName")
    public String updateFirstName(@RequestParam(name = "firstName") String firstName, Principal principal){
        userService.updateUserFirstName(principal.getName(), firstName);
        return "ok";
    }

    @PutMapping("/api/user/lastName")
    public String updateLastName(@RequestParam(name = "lastName") String lastName, Principal principal){
        userService.updateLastName(principal.getName(), lastName);
        return "ok";
    }

    @PutMapping("/api/user/patronymic")
    public String updatePatronymic(@RequestParam(name = "patronymic") String patronymic, Principal principal){
        userService.updatePatronymic(principal.getName(), patronymic);
        return "ok";
    }

    @PutMapping("/api/user/phoneNumber")
    public String updatePhoneNumber(@RequestParam(name = "phoneNumber") String phoneNumber, Principal principal){
        userService.updatePhoneNumber(principal.getName(), phoneNumber);
        return "ok";
    }

    @PutMapping("/api/user/department")
    public String updateDepartment(@RequestParam(name = "department") String department, Principal principal){
        userService.updateDepartment(principal.getName(), department);
        return "ok";
    }
}
