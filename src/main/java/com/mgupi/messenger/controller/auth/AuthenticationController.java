package com.mgupi.messenger.controller.auth;

import com.mgupi.messenger.dto.auth.AuthRequestDto;
import com.mgupi.messenger.dto.auth.AuthResponseDto;
import com.mgupi.messenger.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class AuthenticationController {

    private final UserService userService;

    @PostMapping("/auth")
    @ResponseStatus(HttpStatus.OK)
    public AuthResponseDto createAuthenticationToken(@RequestBody AuthRequestDto authRequestDto) {
        return userService.findUserByLoginAndPassword(authRequestDto.getUsername(), authRequestDto.getPassword());
    }

}
