package com.mgupi.messenger.controller.auth;

import com.mgupi.messenger.dto.registration.RegistrationRequestDto;
import com.mgupi.messenger.dto.registration.RegistrationResponseDto;
import com.mgupi.messenger.entity.CustomUser;
import com.mgupi.messenger.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @PostMapping("/registration")
    public RegistrationResponseDto createNewUser(@RequestBody RegistrationRequestDto registrationRequestDto){
        CustomUser customUser = userService.registrationNewUser(registrationRequestDto);

        return new RegistrationResponseDto(customUser.getUsername(),
                customUser.getFirstName(), customUser.getLastName(),
                customUser.getPatronymic(), customUser.getPhoneNumber());
    }

    @GetMapping("/registration/checkUsername/{username}")
    public boolean checkUserByUsername(@PathVariable(name = "username") String username){
        // if username is free - return true
        // if username is zanyat - return false
        return userService.checkUserByUsername(username);
    }

}
