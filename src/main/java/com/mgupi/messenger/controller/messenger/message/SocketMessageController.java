package com.mgupi.messenger.controller.messenger.message;

import com.mgupi.messenger.dto.message.MessageSendDto;
import com.mgupi.messenger.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
@AllArgsConstructor
public class SocketMessageController {

    private final MessageService messageService;

    @MessageMapping("/chat")
    @ResponseStatus(HttpStatus.OK)
    public void sendMessage(@Payload MessageSendDto chatMessage,
                            @Header(name = "Authorization") String jwtTokenWithBearer) {
        messageService.sendMessage(chatMessage, jwtTokenWithBearer);
    }
}



















