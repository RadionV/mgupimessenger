package com.mgupi.messenger.controller.messenger.message;

import com.mgupi.messenger.dto.message.MessageReceiveDto;
import com.mgupi.messenger.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.UUID;

@RestController
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/api/message/{id}")
    public MessageReceiveDto getMessage(@PathVariable(name = "id") UUID id, Principal principal){
        return messageService.getMessage(id, principal.getName());
    }

}
