package com.mgupi.messenger.controller.messenger.chat;

import com.mgupi.messenger.dto.chat.ChatInfoDto;
import com.mgupi.messenger.dto.message.MessageReceiveDto;
import com.mgupi.messenger.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class ChatController {

    private final ChatService chatService;

    @GetMapping("/api/messenger/chats")
    public List<ChatInfoDto> getAllChats(Principal principal){
        return chatService.findAllChatByUser(principal.getName());
    }

    @GetMapping("/api/messenger/chats/{id}/messages")
    public List<MessageReceiveDto> getAllMessagesByChat(@PathVariable(name = "id") UUID id, Principal principal){
        return chatService.getAllMessagesByChat(id, principal.getName());
    }

    @GetMapping("/api/messenger/chats/{id}/messages/page/{page}")
    public List<MessageReceiveDto> getAllMessagesByChatPage(@PathVariable(name = "id") UUID id,
                                                        Principal principal,
                                                        @PathVariable(name = "page") Integer page){
        return chatService.getPageMessagesByChat(id, page, principal.getName());
    }
}



















