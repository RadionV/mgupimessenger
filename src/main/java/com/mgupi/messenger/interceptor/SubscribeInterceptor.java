package com.mgupi.messenger.interceptor;

import com.mgupi.messenger.exceptions.AccessDeniedException;
import com.mgupi.messenger.exceptions.BadJwtTokenException;
import com.mgupi.messenger.service.CustomUserDetailsService;
import com.mgupi.messenger.service.JWTUtil;
import com.mgupi.messenger.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class SubscribeInterceptor implements ChannelInterceptor {

    private final JWTUtil jwtUtil;
    private final CustomUserDetailsService customUserDetailsService;
    private final MessageService messageService;

    @Autowired
    public SubscribeInterceptor(JWTUtil jwtUtil, CustomUserDetailsService customUserDetailsService, @Lazy MessageService messageService) {
        this.jwtUtil = jwtUtil;
        this.customUserDetailsService = customUserDetailsService;
        this.messageService = messageService;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {

        final StompHeaderAccessor headerAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if(headerAccessor != null){
            if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
                    String jwtToken;
                    String subscribeDestination;

                try{
                    jwtToken = headerAccessor.getNativeHeader("Authorization").get(0).substring(7);
                    subscribeDestination  = headerAccessor.getNativeHeader("destination").get(0);
                }
                catch (NullPointerException e) {
                    throw new NullPointerException("Something went wrong with get token or parse destination");
                }

                if(!jwtUtil.validateToken(jwtToken)){
                    throw new BadJwtTokenException();
                }

                if(!subscribeDestination.equals("/user/"+ jwtUtil.getLoginFromToken(jwtToken) + "/messages") ){
                    throw new AccessDeniedException();
                }

                UserDetails userDetails = customUserDetailsService.loadUserByUsername(jwtUtil.getLoginFromToken(jwtToken));
                final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                headerAccessor.setUser(authentication);

                System.out.println("Subscribe! User : " + jwtUtil.getLoginFromToken(jwtToken) + " To: " + "/user/"+ jwtUtil.getLoginFromToken(jwtToken) + "/messages");

            }
        }

        return message;
    }

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

        final StompHeaderAccessor headerAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if(headerAccessor != null){
            if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
                System.out.println("After subscribe");
                String jwtToken;
                try{
                    jwtToken = headerAccessor.getNativeHeader("Authorization").get(0).substring(7);
                }
                catch (NullPointerException e) {
                    throw new NullPointerException("Something went wrong with get token or parse destination");
                }


                messageService.sendAllNotDeliveredMessage(jwtUtil.getLoginFromToken(jwtToken));
            }
        }

    }
}
