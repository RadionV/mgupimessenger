package com.mgupi.messenger.interceptor;

import com.mgupi.messenger.service.CustomUserDetailsService;
import com.mgupi.messenger.service.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class ConnectInterceptor implements ChannelInterceptor {

    private final CustomUserDetailsService customUserDetailsService;
    private final JWTUtil jwtUtil;

    @Autowired
    public ConnectInterceptor(CustomUserDetailsService customUserDetailsService, JWTUtil jwtUtil) {
        this.customUserDetailsService = customUserDetailsService;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {

        final StompHeaderAccessor headerAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if(headerAccessor != null){
            if (StompCommand.CONNECT.equals(headerAccessor.getCommand())) {
                UserDetails userDetails = customUserDetailsService.loadUserByUsername(jwtUtil.getLoginFromToken(headerAccessor.getNativeHeader("login").get(0).substring(7)));
                final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                headerAccessor.setUser(authentication);
                System.out.println("Connected user: " + authentication.getName());

            }
        }

        return message;
    }

}
