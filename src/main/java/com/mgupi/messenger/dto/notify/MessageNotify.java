package com.mgupi.messenger.dto.notify;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class MessageNotify {
    private UUID messageId;
}
