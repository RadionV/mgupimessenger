package com.mgupi.messenger.dto.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageSendDto {
    private String content;
    private String recipient;
}

