package com.mgupi.messenger.dto.message;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
//@AllArgsConstructor
public class MessageReceiveDto {
//    private UUID id;
    private UUID senderId;
    private Date sendTime;
    private String content;
    private UUID chatId;
}
