package com.mgupi.messenger.dto.chat;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.UUID;

@Setter
@Getter
public class ChatInfoDto {
    UUID chatId;
    UUID userId;
    String firstName;
    String lastName;
    String patronymic;
    String username;
}
