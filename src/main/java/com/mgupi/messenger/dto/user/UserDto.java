package com.mgupi.messenger.dto.user;

import lombok.Data;

@Data
public class UserDto {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String phoneNumber;
    private String department;
}
